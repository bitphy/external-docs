# The BitPhy Docs

This is the public documentation for BitPhy's partners and SaaS customers. The 
main means of interaction is BitPhy's REST API.

## BitPhy API

1. [General API setup](src/bitphy-api-json/general-API-setup.md)
2. [Raw data models](src/bitphy-api-json/data-models.md)
3. Clean data models (coming soon).
4. [Writing data](src/bitphy-api-json/writing-data.md).
5. Reading data (coming soon).
