# BitPhy API Raw Data Models

In this document we describe the _raw_ models in the API, that is: the models with
which it is necessary to deal in order to add the tickets from a shop to our 
system.

In order to have access to the endpoint, it is necessary to know the shop's UUID, 
denoted as `SHOP_ID` below.

* **Product** (https://api.testing.bitphy.es/api/shops/SHOP_ID/products). It 
contains the list of products related to a given shop. Relevant fields:

    * ``shopId`` (str). Relates to the field ``id`` in the **Shop** model. It is 
    the UUID of the shop to which the product belongs.
    
    * ``originalId`` (str). The _id_ associated to this product in 
    the Shop's own data storage.
    
    * ``name`` (str). Product's name to display in the Dashboard.

    * ``id`` (str). Identifier for this product within the system. Generated 
    automatically when posting the new product by concatenating the ``shopId`` and
     ``originalId`` separated by a ``:``, as follows:
     
     ```
     shopId = "ffffffff-ffff-ffff-ffff-ffffffffffff"
     
     originalId = "413"
     
     id = "ffffffff-ffff-ffff-ffff-ffffffffffff:413"
     ```

* **Tickets** (https://api.testing.bitphy.es/api/shops/SHOP_ID/tickets). It contains the *header* information for tickets, notably: the 
ticket ID, a timestamp and the total billing. Relevant fields:

    * ``shopId`` (str). Relates to the field ``id`` in the **Shop** model. It is 
    the UUID of the shop to which the ticket belongs.
    
    * ``originalId`` (int). The _id_ associated to this ticket in the shop's data 
    storage.
    
    * ``timestamp`` (UTC datetime). The time at which the purchase took place.
    
    * ``billing`` (float). The total amount of the purchase.
    
    * ``tpv`` (int, optional). An identifier for the shop's POS device where the 
    ticket was recorded, if known.


* **TicketProduct** (https://api.testing.bitphy.es/api/shops/SHOP_ID/ticketProducts).
It contains the information related to ticket *lines*, that is: which individual 
products are part of the purchase, the amounts of product (either in units or 
weight) which were sold and the billing for that specific product.

    * ``shopId`` (str). Matches the `id` field in the **Shop** model. It is the shop
     to which this ticket line belongs.
    * ``productId`` (str). Matches the `id` field in the **Product** model. It is 
    the product corresponding to this ticket line.
    * ``ticketsId`` (int). Matches the `originalId` in the **Tickets** model. It 
    is the id of the ticket to which the line belongs.
    * ``productNum`` (int), ``grams`` (float) (only one needs to be given). 
    Quantity of product sold. Either in units in the first case or in grams in the
    second case.
    * ``billing`` (float). The amount paid for this specific product and amount.
