## 1. General purpose

Our API is built by using the [LoopBack API 3.18.3](https://loopback.io/) 
framework. As such, the following documentation resources are available:
 
* **Loopback API Explorer**. It contains the list of the data models which are 
exposed by the API; the list of available operations for each model is described.
 https://api.testing.bitphy.es/explorer/
 
* **Domain Model Visualizer**. It exposes a visualization of the different data 
models, their properties and the existing relationships amongst them. 
https://api.testing.bitphy.es/visualizer

* **Loopback Documentation**. http://loopback.io/doc/en/lb3/

## 2. Authentication

In order to make successful requests to the API, it is necessary to have a shop ID 
and an authentication token.

The token can be passed as the ``Authentication`` field of the request HTTP 
headers.

The shop ID is a UUID v4 code which identifies the shop for all purposes within 
our system. It is necessary because the base url for requests related to a given shop is
```html
https://api.testing.bitphy.es/shops/SHOP_ID/
```

## 3. String encoding

Strings are stored in our database using the ``ISO-8859-1`` encoding (also known
as ``latin-1``) and we recommend using this encoding when posting data to the API 
in order to avoid encoding errors of special characters.

## 4. General recommendations

At BitPhy we have made extensive use of this API in production. Our general 
recommendations if massive data ingestions need to be carried out are the 
following:

* The API instance has no limit of petitions per minute. Instead, the amount of 
resources dedicated to it are limited and bounded. In practice this means that the
API can be easily collapsed and can start returning 500 "Internal Server Error" 
status codes while it restarts. Most of our subsequent recommendations are about
being nice and avoiding collapsing the API.

* Use a retry policy by verifying the HTTP status code and using exponential 
backoff.

* Break big POST requests into chunks of small size in order to avoid 413 "Payload
 too large" errors.
 
 If the resources allocated to the API are insufficient for your needs, please 
 contact us and request a resource upgrade.
 
* A swagger specification for the API is available at 
https://api.testing.bitphy.es/explorer/swagger.json; this specification can be 
passed to [swagger-codegen](https://github.com/swagger-api/swagger-codegen) in 
order to generate client libraries in several languages.

 
### 4.1 Recommendations for Python scripts

If you are planning to use Python for a data ingestion script, then we can give 
more specific advice.

First of all, we recommend the use of the ``requests`` library 
(http://docs.python-requests.org/en/master/). 

For large data ingestions, we have found the use of ``requests`` session objects 
(http://docs.python-requests.org/en/master/user/advanced/#session-objects) to 
present a significant efficiency gain.

Finally, for **very** large data ingestions, and provided you let us know and we put 
enough API instances and resources, it is a good idea to use asynchronous 
concurrent threads; such funcionality is provided, for example, by the ``aiohttp`` 
library (https://aiohttp.readthedocs.io/en/stable/).
 

