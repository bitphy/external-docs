# Writing data into BitPhy API

We will explain how to extract the data from a ticket and introduce it into the 
system. In order to properly describe the process, we will use the example of a 
physical ticket.

## Preliminaries

Before introducing a ticket, you should be in possession of an authentication 
token and a shop Id. We will assume that we want to introduce a ticket belonging 
to a shop whose ``id`` is ``ffffffff-ffff-ffff-ffff-ffffffffffff`` and that our 
authentication token is ``foobar``.

## An example ticket

The ticket below belongs to a bar. We will use it as an example, describe the data
contained in it and encode it in JSON.

![An example ticket](https://bitbucket.org/bitphy/external-docs/raw/8a5c7a33da0342f37522f3367f09afeafca37e32/src/bitphy-api-json/png/ticket.png)

### Products in the ticket

It is possible to introduce a list of products into the **Products** model if such a
list is available beforehand. In this way, there is no need to POST products at 
the same time as POSTing new tickets.

If such a list is not available and the tickets contain enough information about 
the individual product, namely an id code, it is necessary to check whether the 
product exists in the **Product** model and POST otherwise.

Finally, if the ID codes of products are unknown, we recommend the use of incremental 
integers to store them into the **Product** model.

Regarding the ticket at hand, we will use the latter procedure. In JSON notation:

```json
[
  {
    "name": "Coca cola",
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
    "originalId": "1"
  },
  {
    "name": "Fanta limón",
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
    "originalId": "2"
  },
  {
    "name": "Sprite",
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
    "originalId": "3"
  }
]
```

After a POST request to the https://api.testing.bitphy.es/api/products endpoint, we would 
retrieve the following content as a response (note the automatically generated ``id``):


```json
[
  {
    "id": "ffffffff-ffff-ffff-ffff-ffffffffffff:1",
    "name": "Coca cola",
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
    "originalId": "1",
    "pricingClassification": -1,
    "unit": null
  },
  {
    "id": "ffffffff-ffff-ffff-ffff-ffffffffffff:2",
    "name": "Fanta limón",
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
    "originalId": "2",
    "pricingClassification": -1,
    "unit": null
  },
  {
    "id": "ffffffff-ffff-ffff-ffff-ffffffffffff:3",
    "name": "Sprite",
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
    "originalId": "3",
    "pricingClassification": -1,
    "unit": null
  }
]
```

### Global Ticket Data

Here we will encode the global information related to the ticket itself. Remember 
to convert local _naive_ datetimes to UTC:

```json
{
  "originalId": 11,
  "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff",
  "timestamp": "2011-06-13T13:20:00.000Z",
  "billing": 10.00
}
```

The above should be used as payload in a POST request to the 
https://api.testing.bitphy.es/Tickets endpoint.


### Ticket lines

The following three lines related to products sold in the ticket may be extracted:
```json
[
  {
    "productNum": 2,
    "billing": 5.00,
    "grams": null,
    "productId": "ffffffff-ffff-ffff-ffff-ffffffffffff:1",
    "ticketsId": 11,
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff"
  },
  {
    "productNum": 1,
    "billing": 2.50,
    "grams": null,
    "productId": "ffffffff-ffff-ffff-ffff-ffffffffffff:2",
    "ticketsId": 11,
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff"
  },
  {
    "productNum": 1,
    "billing": 2.50,
    "grams": null,
    "productId": "ffffffff-ffff-ffff-ffff-ffffffffffff:3",
    "ticketsId": 11,
    "shopId": "ffffffff-ffff-ffff-ffff-ffffffffffff"
  }
]

```

The above JSON string can be used as payload for a single POST request to the 
https://api.testing.bitphy.es/api/TicketProducts endpoint.

## An example program

The Python 3.6 code below uploads the given example ticket to the API by using the  
``requests`` library (http://docs.python-requests.org/en/master/).

```python
import requests

# We recommend to store these as an env variables.
TOKEN = "foobar"
SHOP_ID = "ffffffff-ffff-ffff-ffff-ffffffffffff"
API_BASE_URL = f"https://api.testing.bitphy.es/api/shops/{SHOP_ID}"

HEADERS = {'Authorization': TOKEN,
           'cache-control': 'no-cache'}

products = [
    {
        "name": "Coca cola",
        "shopId": SHOP_ID,
        "originalId": "1"
    },
    {
        "name": "Fanta limón",
        "shopId": SHOP_ID,
        "originalId": "2"
    },
    {
        "name": "Sprite",
        "shopId": SHOP_ID,
        "originalId": "3"
    }
]

ticket_head = {
    "originalId": 11,
    "shopId": SHOP_ID,
    "timestamp": "2011-06-13T13:20:00.000Z",
    "billing": 10.00,
}

ticket_lines = [
    {
        "productNum": 2,
        "billing": 5.00,
        "grams": None,
        "productId": f"{SHOP_ID}:1",
        "ticketsId": 11,
        "shopId": SHOP_ID
    },
    {
        "productNum": 1,
        "billing": 2.50,
        "grams": None,
        "productId": f"{SHOP_ID}:2",
        "ticketsId": 11,
        "shopId": SHOP_ID
    },
    {
        "productNum": 1,
        "billing": 2.50,
        "grams": None,
        "productId": f"{SHOP_ID}:3",
        "ticketsId": 11,
        "shopId": SHOP_ID
    }
]

# Upload new products
prods_url = f"{API_BASE_URL}/products"
response_p = requests.post(prods_url, json=products, headers=HEADERS)

# Upload ticket header
tickets_url = f"{API_BASE_URL}/Tickets"
response_t = requests.post(tickets_url, json=ticket_head, headers=HEADERS)

# Upload new products
ticket_prods_url = f"{API_BASE_URL}/TicketProducts"
response_tp= requests.post(ticket_prods_url, json=ticket_lines, headers=HEADERS)

# Information about request success can be accessed as follows
print(
    f"Products | Status code {response_p.status_code} | Body {response_p.text}"
)
print(
    f"Ticket head | Status code {response_t.status_code} | Body {response_t.text}"
)
print(
    f"Ticket lines | Status code {response_tp.status_code} | Body {response_tp.text}"
)

```



